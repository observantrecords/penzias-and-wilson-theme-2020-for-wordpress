# Penzias and Wilson 2020 Theme

A custom theme for [Penzias and Wilson](http://penziasandwilson.com/).

## Dependencies

* [Observant Records 2020 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2020-for-wordpress)

### Observant Records Artist Connector

The `archive-album`, `content-album` and `content-track` templates query the Observant Records artist database for discography data.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-artist-connector-for-wordpress)
for usage.

### Observant Records Blocks

This plugin provides custom blocks for the Gutenberg editor.

See the [plugin documentation](https://bitbucket.org/observantrecords/observant-records-blocks-for-wordpress)
for usage.
